resource "aws_dynamodb_table" "dynamodb_table_a" {
  name           = "tf-aws-dynamodb-backup-table-a"
  read_capacity  = 1
  write_capacity = 1
  hash_key       = "itemId"

  attribute {
    name = "itemId"
    type = "S"
  }
}

resource "aws_dynamodb_table" "dynamodb_table_b" {
  name           = "tf-aws-dynamodb-backup-table-b"
  read_capacity  = 1
  write_capacity = 1
  hash_key       = "itemId"

  attribute {
    name = "itemId"
    type = "S"
  }
}

module "dynamodb_table_backup" {
  source      = "../"

  tables      = ["${aws_dynamodb_table.dynamodb_table_a.name}", "${aws_dynamodb_table.dynamodb_table_b.name}"]
  retain_days = 7
  schedule    = "5 minutes"
}
