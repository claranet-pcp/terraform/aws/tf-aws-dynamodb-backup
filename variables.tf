variable "tables" {
  type = "list"
  description = "List of DynamoDB table names"
}

variable "retain_days" {
  type = "string"
  description = "Days to retain previous backups"
  default = "7"
}

variable "schedule" {
  type = "string"
  description = "describe your variable"
  default = "12 hours"
}

variable "function_name" {
  type        = "string"
  description = "Lambda function name"
  default     = "tf-aws-dynamodb-backup"
}
