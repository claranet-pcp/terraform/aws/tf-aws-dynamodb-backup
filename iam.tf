# Lambda role
resource "aws_iam_role" "dynamodb_backup_lambda_role" {
  name_prefix = "dynamodb_backup_role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "cloudwatch_logs" {
  name = "cloudwatch-logging"
  role = "${aws_iam_role.dynamodb_backup_lambda_role.id}"
  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "logs:*"
            ],
            "Resource": [
                "arn:aws:logs:*:*:*"
            ]
        }
    ]
}
EOF
}

resource "aws_iam_role_policy" "dynamodb_table_backup" {
  name = "dynamodb-table-backup"
  role = "${aws_iam_role.dynamodb_backup_lambda_role.id}"
  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "dynamodb:CreateBackup",
                "dynamodb:ListBackups",
                "dynamodb:DeleteBackup"
            ],
            "Resource": [
              "*"
            ]
        }
    ]
}
EOF
}

resource "aws_lambda_permission" "allow_cloudwatch_execute_lambda" {
    statement_id  = "AllowExecutionFromCloudWatch"
    action        = "lambda:InvokeFunction"
    function_name = "${aws_lambda_function.dynamodb_backup_lambda.function_name}"
    principal     = "events.amazonaws.com"
    source_arn    = "${aws_cloudwatch_event_rule.dynamodb_backup_schedule.arn}"
}
