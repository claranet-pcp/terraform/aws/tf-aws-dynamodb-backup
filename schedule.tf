resource "aws_cloudwatch_event_rule" "dynamodb_backup_schedule" {
    name = "dynamodb-backup-schedule"
    description = "Execute DynamoDB Backup on schedule"
    schedule_expression = "rate(${var.schedule})"
}

resource "aws_cloudwatch_event_target" "dynamodb_backup_schedule" {
    rule = "${aws_cloudwatch_event_rule.dynamodb_backup_schedule.name}"
    target_id = "dynamodb_backup_lambda"
    arn = "${aws_lambda_function.dynamodb_backup_lambda.arn}"
}
