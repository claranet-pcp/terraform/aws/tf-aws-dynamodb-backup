# tf-aws-dynamodb-backup

Module to deploy a lambda function which performs DynamoDB baclups on a schedule (oddly not built in to AWS)

## Usage

```
module "dynamodb_table_backup" {
  source      = "git@gogs.bashton.net:Bashton-Terraform-Modules/tf-aws-dynamodb-backup.git"

  tables      = ["${aws_dynamodb_table.dynamodb_table_a.name}", "${aws_dynamodb_table.dynamodb_table_b.name}"]
  retain_days = 7
  schedule    = "5 minutes"
}
```

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| function_name | Lambda function name | string | `tf-aws-dynamodb-backup` | no |
| retain_days | Days to retain previous backups | string | `7` | no |
| schedule | describe your variable | string | `12 hours` | no |
| tables | List of DynamoDB table names | list | - | yes |
