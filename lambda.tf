## create lambda package
data "archive_file" "dynamodb_backup_lambda_package" {
  type        = "zip"
  source_file = "${path.module}/lambda.py"
  output_path = "${path.cwd}/.terraform/tf-aws-dynamodb-backup-${md5(file("${path.module}/lambda.py"))}.zip"
}

## create lambda function
resource "aws_lambda_function" "dynamodb_backup_lambda" {
  filename         = "./.terraform/tf-aws-dynamodb-backup-${md5(file("${path.module}/lambda.py"))}.zip"
  source_code_hash = "${data.archive_file.dynamodb_backup_lambda_package.output_base64sha256}"
  function_name    = "${var.function_name}"
  role             = "${aws_iam_role.dynamodb_backup_lambda_role.arn}"
  handler          = "lambda.handler"
  runtime          = "python2.7"
  timeout          = "60"

  environment {
    variables = {
      TABLES      = "${join(",", var.tables)}"
      RETAIN_DAYS = "${var.retain_days}"
    }
  }
}
