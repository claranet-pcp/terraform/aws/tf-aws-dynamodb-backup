from __future__ import print_function
from datetime import date, datetime, timedelta
import json
import boto3
import time
from botocore.exceptions import ClientError
import os

dynamo_tables = os.environ['TABLES'].split(",")
retain_days  = int(os.environ['RETAIN_DAYS'])

dynamo = boto3.client('dynamodb')

def handler(event, context):
  try:
    for dynamo_table in dynamo_tables:

      # create backup
      dynamo.create_backup(TableName = dynamo_table, BackupName = dynamo_table)
      print('Backup complete for table:', dynamo_table)

      # Find all backups older than `retain_days`
      delete_upper_date = datetime.now() - timedelta(days = retain_days)
      print('Deleting backups prior to ', delete_upper_date)

      # TimeRangeLowerBound is the release of DynamoDB Backup and Restore - Nov 29, 2017
      response = dynamo.list_backups(TableName = dynamo_table,
                                     TimeRangeLowerBound = datetime(2017, 11, 29),
                                     TimeRangeUpperBound = delete_upper_date )

      # remove any backups
      for backup in response['BackupSummaries']:
        dynamo.delete_backup(BackupArn = backup['BackupArn'])
        print('Deleted:', backup['TableName'], backup['BackupCreationDateTime'])

  except  ClientError as e:
    print('Client Error: ', e)

  except ValueError as ve:
    print('Value Error: ', ve)

  except Exception as ex:
    print('Exception: ', ex)
